# This script is an example script for using the hdf_utils func library to read from the
# HDF5 files.

import h5py
import hdf_utils as hdfutils


def run_readin(hdf_fname, snap):
    """Run example reading in algorithms"""

    # Open HDF5 file,
    hf = h5py.File(hdf_fname, 'r')

    # Print header information
    hdfutils.print_headergrp_attrs(hf)

    # Read snapshot halo catalog as pandas dataframe
    pd_catalog = hdfutils.read_halo_catalog(hf, snap)

    # Read snapshot halo catalog as numpy matrix
    # Less clear but quicker read in
    numpy_catalog = hdfutils.read_halo_cat_numpy(hf, snap)

    # Read halo-particle information as dictionary
    # Format = parts_info['ids'][0,1,2,...,NHalos] = np.array([])
    parts_info = hdfutils.read_snap_particle_info(hf, snap)

    # Read tree data as pandas dataframe.
    tree = hdfutils.read_tree_as_df(hf)

    return hf, tree, pd_catalog, numpy_catalog, parts_info


"""Main program."""

# Path to HDF5 and snapshot you want to read in.
hdf_fname = '/path/to/.hdf5'
snap = 128

# Run read ins.
hf, tree, pd_catalog, numpy_catalog, parts_info = run_readin(hdf_fname, snap)
