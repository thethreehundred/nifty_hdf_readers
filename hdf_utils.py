# Function library containing reading routines from VELOCIraptor hdf5 files.

import time
import numpy as np
import pandas as pd
import h5py


def print_headergrp_attrs(hf):
    """Print attributes in header group and sub groups"""

    # Get group
    grp = hf['Header/']

    # Print for mother group
    for key, value in grp.attrs.items():
        print(key, ':', value)
    # Find subgroups and print for these.
    for i in grp.__iter__():
        subgrp = grp.__getitem__(i)
        for key, value in subgrp.attrs.items():
            print(key, ':', value)
    print('\n')


def read_halo_catalog(hf, snap, h=0.7):
    """ Read snapshot halo catalog as pandas data frame.
    Use if need column header names, to rescale units. This is slower
    then a numpy slice.

    POSSIBLE ERROR: if catalog is empty, this will error. Will fix."""

    # Get group
    grp = hf['Snapshots/snap_' + str('%03d' % snap) + '/']

    # Read fieldnames
    names = grp.attrs.__getitem__('FieldNames')
    fieldnames = names.split()

    # Read data
    cat = grp['HaloCatalog'][:]
    catalog = pd.DataFrame(data=cat, columns=fieldnames)

    # Rescale units, datatypes and put in little h.
    catalog['ID'] = np.int64(catalog['ID'])
    catalog['hostHaloID'] = np.int64(catalog['hostHaloID'])
    catalog['TreeIndex'] = np.int64(catalog['TreeIndex'])
    catalog['Xc'] = catalog['Xc'] * h
    catalog['Yc'] = catalog['Yc'] * h
    catalog['Zc'] = catalog['Zc'] * h
    catalog['VXc'] = catalog['VXc'] * h
    catalog['VYc'] = catalog['VYc'] * h
    catalog['VZc'] = catalog['VZc'] * h
    catalog['Rvir'] = catalog['Rvir'] * h
    catalog['Mvir'] = catalog['Mvir'] * 1e10
    catalog['M_gas'] = catalog['M_gas'] * 1e10
    catalog['M_gas_Rvmax'] = catalog['M_gas_Rvmax'] * 1e10
    catalog['M_gas_30kpc'] = catalog['M_gas_30kpc'] * 1e10
    catalog['M_star'] = catalog['M_star'] * 1e10
    catalog['M_star_Rvmax'] = catalog['M_star_Rvmax'] * 1e10
    catalog['M_star_30kpc'] = catalog['M_star_30kpc'] * 1e10
    return catalog


def read_halo_cat_numpy(hf, snap, read_into_numpy=1):
    """Read halo catalog, using a quick numpy slice."""

    # Slice the catalog data set.
    grp = hf['Snapshots/snap_' + str('%03d' % snap) + '/']
    catalog = grp['HaloCatalog']

    # Read into preexsiting numpy array if there will be lots of fancy
    # indexing. It will be faster.
    num_cols = 171
    if read_into_numpy:
        catalog_size = catalog.size
        num_rows = np.int64(catalog_size / num_cols)
        preall_catalog = np.zeros([num_rows, num_cols])
        catalog.read_direct(preall_catalog)
        catalog = preall_catalog

    return catalog


def read_tree_as_df(hf):
    """ Read tree as pandas dataframe. Takes ~20 seconds.
    Use ipython to store in memory, so don't read every time"""

    # Get group
    grp = hf['Tree/']

    # Get tree
    print('Reading hdf5 tree')
    t0 = time.time()
    j = 1
    for i in grp.__iter__():
        if j == 1:
            tree = pd.DataFrame({i: grp[i][:]})
        else:
            container = pd.DataFrame({i: grp[i][:]})
            tree = pd.concat([tree, container], axis=1)
        j += 1
    print('Time taken to read in tree:', time.time() - t0, 's')
    return tree


def read_snap_particle_info(hf, snap):
    """Extract particle information for specific snap and store
    in a dictionary format"""

    # Read data and Nhalos
    grp = hf['Particles/snap_' + str('%03d' % snap) + '/']
    nhalos = int(grp.attrs.__getitem__('NHalos'))
    group_info = grp['GroupInfo']
    partids = grp['PartID']
    ptypes = grp['PType']

    # Create dictionary for particle info
    parts_info = {'ids': [np.array([]) for i in range(0, nhalos)],
                  'types': [np.array([]) for i in range(0, nhalos)]}

    # Extract information on which halos correspond to which particles.
    size_halos = group_info[0:nhalos]
    part_offset = group_info[nhalos:int(2 * nhalos)]

    # Store particle ids and types.
    for i in range(0, nhalos):
        start = part_offset[i]
        end = part_offset[i] + size_halos[i]
        parts_info['ids'][i] = partids[start:end]
        parts_info['types'][i] = ptypes[start:end]

    return parts_info


def printname(name):
    """ Printing function to be used in tandem with .visit method"""
    print(name)


def read_reduced_gas_part_data(hf, cluster_name, snap, gas, verbose=0):
    """From the reduced data file (which includes dm data at various snaps
    or various clusters) read the dark matter data into memory."""

    t0 = time.time()

    # Preallocate particle info dictionary.
    part_info = {'x': np.array([]),
                 'y': np.array([]),
                 'z': np.array([]),
                 'vx': np.array([]),
                 'vy': np.array([]),
                 'vz': np.array([]),
                 'rho': np.array([]),
                 'u': np.array([]),
                 'ne': np.array([]),
                 'nh': np.array([])}

    # Access group.
    grp = hf['Simulations/' + cluster_name +
             '/snap_' + str('%03d' % snap) + '/']

    # Read slice data
    x = grp['x']
    y = grp['y']
    z = grp['z']
    vx = grp['vx']
    vy = grp['vy']
    vz = grp['vz']

    # Correct little h scaling and store in dict
    h = 0.7
    part_info['x'] = x[:]
    part_info['y'] = y[:]
    part_info['z'] = z[:]
    part_info['vx'] = vx[:]
    part_info['vy'] = vy[:]
    part_info['vz'] = vz[:]

    # If reading in gas, need more quanities.
    if gas:
        rho = grp['rho']
        u = grp['u']
        ne = grp['ne']
        nh = grp['nh']
        part_info['rho'] = rho[:]
        part_info['u'] = u[:]
        part_info['ne'] = ne[:]
        part_info['nh'] = nh[:]

    if verbose:
        print('Particle read in time:', time.time() - t0, 's')
    return part_info


def get_redshift_from_snap(fname, snap):
    """User inputs snapshot number and redshift pops out. Uses seperate file."""

    # Read file
    df = pd.read_csv(fname, sep='\s+', comment='#', header=None)
    df.columns = ['n', 'z', 'a']

    # Get redshift
    redshift = df['z'][snap]
    return redshift


def get_entire_snap_z_file(fname):
    """Read in entire snapshot vs z lookup table"""

    # Read file
    df = pd.read_csv(fname, sep='\s+', comment='#', header=None)
    df.columns = ['n', 'z', 'a']

    return df
